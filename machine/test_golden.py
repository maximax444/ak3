import io
import os
import tempfile
import contextlib
import pytest
from machine.simulation import simulation
from machine.isa import OperandType
from machine.simulation import DataPath, ControlUnit
from machine.translator import parse_data_block, parse_text_block, RealInstruction, RealOperand, parse_code
import json
from json import dumps


@pytest.mark.golden_test("../golden/*.yml")
def test_whole_by_golden(golden):
    # Создаём временную папку для тестирования приложения.
    with tempfile.TemporaryDirectory() as tmpdirname:
        source = os.path.join(tmpdirname, "source.asm")
        input_stream = os.path.join(tmpdirname, "input.txt")
        target = os.path.join(tmpdirname, "target.out")

        # Записываем входные данные в файлы. Данные берутся из теста.
        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_stream, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        # Запускаем транслятор и собираем весь стандартный вывод в переменную
        # stdout
        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            with open(source, "rt", encoding="utf-8") as s1:
                ss = s1.readlines()
            parsed_data, parsed_code = parse_code(ss)
            code = dumps({
                'data': parsed_data,
                'code': parsed_code
            }, indent=4)
            print('machine started')
            print(code)
            program = json.loads(code)
            with open(input_stream, "rt", encoding="utf-8") as s21:
                iss = s21.read()
            print(simulation(program, "", 32, 1e4))

        # Проверяем, что ожидания соответствуют реальности.
        assert code == golden.out["code"]
        assert stdout.getvalue() == golden.out["output"]
